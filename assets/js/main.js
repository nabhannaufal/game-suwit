let batu = document.getElementById("batu");
let kertas = document.getElementById("kertas");
let gunting = document.getElementById("gunting");
let winBoard = document.getElementById("versus-h1");
let resetButton = document.getElementById("reset");
let activeClass = document.getElementsByClassName("active");
let icon = ["batu", "kertas", "gunting"];
let resultTextBegin = 'result-begin';
let comScore = document.getElementById("com_score")
let playerScore = document.getElementById("player_score")
let drawScore = document.getElementById("draw_score")
let crownPlayer = document.getElementById("crown-player")
let crownComputer = document.getElementById("crown-computer")

resetButton.addEventListener("click", function () {
  comScore.textContent = 0;
  playerScore.textContent = 0;
  drawScore.textContent = 0;
  crownPlayer.style.opacity = 0;
  crownComputer.style.opacity = 0;
  reset();

})

function reset() {
  winBoard.className = resultTextBegin;
  kertas.classList.remove("active");
  gunting.classList.remove("active");
  batu.classList.remove("active");

  icon.forEach((item) => {
    let comChoiceClearElement = document.getElementById(`${item}-comp`);
    comChoiceClearElement.classList.remove("active");
  })
}

batu.addEventListener("click", function (e) {
  reset();
  
  if (activeClass.length < 2) {
    batu.classList.add("active");
    kertas.classList.remove("active");
    gunting.classList.remove("active");
    suit('batu');
  }
})

kertas.addEventListener("click", function (e) {
  reset();
  if (activeClass.length < 2) {
    kertas.classList.add("active");
    batu.classList.remove("active");
    gunting.classList.remove("active");
    suit('kertas')
  }
})

gunting.addEventListener("click", function (e) {
  reset();
  if (activeClass.length < 2) {
    gunting.classList.add("active");
    batu.classList.remove("active");
    kertas.classList.remove("active");
    suit('gunting')
  }
})

function suit(choice) {
  if (activeClass.length < 2) {
    let playerChoice = choice;

    let randomNumber = Math.floor(Math.random() * 3);
    let comChoice = icon[randomNumber];

    icon.forEach((item) => {
      let comChoiceClearElement = document.getElementById(`${item}-comp`);
      comChoiceClearElement.classList.remove("active");
    })

    let comChoiceElement = document.getElementById(`${comChoice}-comp`);
    comChoiceElement.classList.add("active");

    let result = winChecking(playerChoice, comChoice);

    if (result === 'result-com-win') {
      let textComScore = parseInt(comScore.textContent);
      comScore.textContent = textComScore + 1;
      crownPlayer.style.opacity = 0;
      crownComputer.style.opacity = 1;
    }

    if (result === 'result-player-win') {
      let textPlayerScore = parseInt(playerScore.textContent);
      playerScore.textContent = textPlayerScore + 1;
      crownPlayer.style.opacity = 1;
      crownComputer.style.opacity = 0;
    }

    if (result === 'result-draw') {
      let textDrawScore = parseInt(drawScore.textContent);
      drawScore.textContent = textDrawScore + 1;
      crownPlayer.style.opacity = 0.2;
      crownComputer.style.opacity = 0.2;
    }

    winBoard.className = result;
  }
}

function winChecking(playerChoice, comChoice) {
  if (playerChoice === comChoice) return 'result-draw';

  if (playerChoice === 'batu' && comChoice === 'kertas') {
    return 'result-com-win';
  }

  if (playerChoice === 'batu' && comChoice === 'gunting') {
    return 'result-player-win';
  }

  if (playerChoice === 'kertas' && comChoice === 'batu') {
    return 'result-player-win';
  }

  if (playerChoice === 'kertas' && comChoice === 'gunting') {
    return 'result-player-win';
  }

  if (playerChoice === 'gunting' && comChoice === 'batu') {
    return 'result-player-win';
  }

  if (playerChoice === 'gunting' && comChoice === 'kertas') {
    return 'result-player-win';
  }
}

